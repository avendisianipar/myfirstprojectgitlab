//: [Previous](@previous)

import Foundation

public class Persegi {
    var sisi: Double
    
    init(sisi: Double) {
        self.sisi = sisi
    }
    
    func getLuas() -> Double {
        return sisi * sisi
    }
}

var persegi = Persegi(sisi: 10.0)
print(persegi.getLuas())


//asdf
