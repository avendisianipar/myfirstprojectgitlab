//: [Previous](@previous)

import Foundation

public class Lingkaran {
    var jari: Double
    var pi = 3.14
    
    init(jari: Double, pi: Double) {
        self.jari = jari
        self.pi = pi
    }
    
    func getLuas() -> Double {
        return pi * jari * jari
    }
}
